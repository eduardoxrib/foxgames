<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>FoxGames</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">FoxGames</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="#"><b class='home'>Home</b></a></li>
                <li><a href="{{ route('jogo.create') }}">Cadastro de <b>Filme</b></a></li>
                <li><a href="#">Cadastro de <b>Idioma</b></a></li>
            </ul> 
        </div>
    </nav>

    <div class="container">
        @yield('conteudo')
    </div>

</body>

    <style>
        .home {
            font-size: large;
        }
    </style>
</html>