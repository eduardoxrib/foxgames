@extends('adminlte::page')

@section('title', 'FoxGames - Categorias')

@section('content_header')

@endsection

@section('content')

    <h1 style="text-align: center; margin-bottom: 2%">{{$jogo->titulo}}</h1>

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="row">
        <div class="col-sm-6">
            <form method="post" action="{{ route('jogo.update', $jogo->id) }}" enctype="multipart/form-data">
                {{ method_field('put') }} 
                {{ csrf_field() }}
                <div class="container-fluid" style="margin-bottom: 5%">
                    <div class="col-sm-6">
                        <label for="titulo">Titulo:</label><br/>
                        <input type="text" name="titulo" id="titulo" value="{{$jogo->titulo}}"><br/>
                        <label for="preco">Preço:</label><br/>
                        <input type="text" name="preco" id="preco" value="{{number_format($jogo->preco, 2)}}">
                    </div>
                    <div class="col-sm-6">
                        <label for="desenvolvedor_id">Desenvolvedor:</label><br/>
                        <select class="form-control" id="desenvolvedor_id" name="desenvolvedor_id">
                            @foreach ($desenvolvedores as $desenvolvedor)
                                <option value="{{$desenvolvedor->id}}">
                                    {{$desenvolvedor->nome}}
                                </option>
                            @endforeach
                        </select>
                        <label for="foto">Foto:</label>
                        <input type="file" class="form-control" id="foto" name="foto">
                    </div>
                </div>

                <div class="col-sm-12" style="text-align: center; margin-bottom: 5%"> <input type="submit" value="Enviar" class="btn btn-success"> </div>
            </form>

            <div class="container-fluid">
                <div class="col-sm-6">
                    <label for="categoria">Adicionar Categoria:</label>
                    <form method="post" action="{{ route('jogo.add_categoria') }}">
                        {{ csrf_field() }}
                        <select class="form-control" 
                                    id="categoria_id" name="categoria_id">
                            @foreach ($categorias as $categoria)
                                <option value="{{$categoria->id}}">
                                    {{$categoria->nome}}
                                </option>
                            @endforeach
                        </select>
                        <input class="form-control" type="hidden" value="{{$jogo->id}}" id="jogo_id" name="jogo_id">
                        <input type="submit" class="btn btn-primary" style="float: right" value="Add">
                    </form>
                </div>
                <div class="col-sm-6">
                    <label for="idioma">Adicionar Idioma:</label>
                    <form method="post" action="{{ route('jogo.add_idioma') }}">
                        {{ csrf_field() }}
                        <select class="form-control" 
                                    id="idioma_id" name="idioma_id">
                            @foreach ($idiomas as $idioma)
                                <option value="{{$idioma->id}}">
                                    {{$idioma->nome}}
                                </option>
                            @endforeach
                        </select>
                        <input class="form-control" type="hidden" value="{{$jogo->id}}" id="jogo_id" name="jogo_id">
                        <input type="submit" class="btn btn-primary" style="float: right" value="Add">
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <table class="table">
                        <thead class="table-dark">
                            <tr>
                                <th>ID</th>
                                <th>Categoria</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($jogo->categorias as $categoria)
                                <tr><th>{{$categoria->nome}}</th></tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6">
                    <table class="table">
                        <thead class="table-dark">
                            <tr>
                                <th>ID</th>
                                <th>Idiomas</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($jogo->idiomas as $idioma)
                                <tr><th>{{$idioma->nome}}</th></tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <img src="../storage/{{ $jogo->foto }}" alt="jogo" style="width: 100%">
        </div>
    </div>

@endsection

@section('js')
  <script src="https://code.jquery.com/jquery-latest.min.js"></script>
  <script src="/js/jquery.mask.min.js"></script>

  <script>
    $(document).ready(function() {
      $('#preco').mask('#.###.##0,00', {reverse: true});
    });
  </script>  
@endsection