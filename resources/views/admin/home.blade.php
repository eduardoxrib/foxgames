@extends('adminlte::page')

@section('title', 'FoxGames')

@section('content_header')

@endsection

@section('content')

    <h1 style="text-align: center">Bem vindo {{Auth::user()->name}}</h1>
    <div class="container">
        <div class="col-sm-6">
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
            <script type="text/javascript">
            google.charts.load("current", {packages:["corechart"]});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var data = google.visualization.arrayToDataTable([['Desenvolvedora', 'Nº de Jogos'],
                    @foreach ($jogos as $jogo)
                        {!! "['$jogo->dev', $jogo->num]," !!}
                    @endforeach
                ]);
                var options = {
                    title: 'Nº de Jogos por Desenvolvedor',
                    is3D: true,
                };
                var chart = new google.visualization
                .PieChart(document.getElementById('piechart_3d'));
                chart.draw(data, options);
            }
            </script>
            <div id="piechart_3d" style="width: 100%; height: 400px"></div>
        </div>

        <div class="col-sm-6">
            <html>
                <head>
                    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                    <script type="text/javascript">
                    google.charts.load('current', {'packages':['table']});
                    google.charts.setOnLoadCallback(drawTable);

                    function drawTable() {
                        var data = new google.visualization.DataTable();
                        data.addColumn('string', 'Nome');
                        data.addColumn('number', 'Valor de Compra');
                        data.addColumn('boolean', 'Pago');
                        data.addRows([

                            @foreach ($vendas as $venda)
                                {!! "['$venda->nome', {v: $venda->preco, f: '$venda->preco'}, true]," !!}
                            @endforeach

                        ]);

                        var table = new google.visualization.Table(document.getElementById('table_div'));

                        table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
                    }
                    </script>
                </head>
            <body>
            <div id="table_div"></div>
            </body>
            </html>
        </div>
    </div>

@endsection