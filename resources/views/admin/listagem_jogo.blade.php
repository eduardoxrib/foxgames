@extends('adminlte::page')

@section('title', 'FoxGames - Jogo')

@section('content_header')

@endsection

@section('content')

    <div class="tela">
    <h1>Jogos Disponíveis</h1>

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="row">
        @foreach ($jogos as $jogo)
            <div class="col-sm-4" style="margin-bottom: 5%">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="{{ asset('storage/'.$jogo->foto) }}" alt="Card image cap" style="width: 320px; height: 280px; align:center">
                    <div class="card-body" style="width: 320px">
                        <h5 class="card-title" style="text-align: center"><b>{{$jogo->titulo}}</b></h5>
                        <p class="card-text" style="text-align: center">{{$jogo->desenvolvedor->nome}}</p>
                        <div style="text-align: center;">
                            <a href="{{ route('jogo.show', $jogo->id) }}" class="btn btn-info">Editar</a>
                            <form method="post" action="{{ route('jogo.destroy', $jogo->id) }}" style="display: inline-block" onsubmit="return confirm('Confirma Exclusão do Jogo?'")>
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <input type="submit" class="btn btn-danger btn-sm" value="Excluir">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row" style="text-align: center">
        {{ $jogos->links() }}
    </div>
    </div>
@endsection