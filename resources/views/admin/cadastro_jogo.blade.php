@extends('adminlte::page')

@section('title', 'FoxGames - Jogo')

@section('content_header')

@endsection

@section('content')

    <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-8">
    <h1 style="text-align: center">Jogos</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <h3>Cadastro de Jogos</h3>
    <form method="post" action="{{ route('jogo.store') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        
        <div class="row">
            <div class="col-sm-6">
                <label for="titulo">Título:</label>
                <div class="form-group">
                    <input type="text" class="form-control" id="titulo" name="titulo">
                </div>
            </div>
            <div class="col-sm-6">
                <label for="nome">Desenvolvedor:</label>
                <select class="form-control" 
                            id="desenvolvedor_id" name="desenvolvedor_id">
                    @foreach ($desenvolvedores as $desenvolvedor)
                        <option value="{{$desenvolvedor->id}}">
                            {{$desenvolvedor->nome}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <label for="preco">Preço:</label>
                <div class="form-group">
                    <input type="text" class="form-control" id="preco" name="preco">
                </div>
            </div>
            <div class="col-sm-4">
                <label for="foto">Foto:</label>
                <div class="form-group">
                    <input type="file" class="form-control" id="foto" name="foto">
                </div>
            </div>
            <div class="col-sm-2">
                <input type="submit" class="btn btn-primary" style="float: right" value="Adicionar">
            </div>
        </div>
        
    </form>
  </div>

@endsection

@section('js')
  <script src="https://code.jquery.com/jquery-latest.min.js"></script>
  <script src="/js/jquery.mask.min.js"></script>

  <script>
    $(document).ready(function() {
      $('#preco').mask('#.###.##0,00', {reverse: true});
    });
  </script>  
@endsection