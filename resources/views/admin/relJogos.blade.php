<div class="container-fluid">
    <h3 style="text-align: center">Fox Games</h3>
    <h5 style="text-align: center">Relatório de Jogos Cadastrados</h5>
    <h5 style="text-align: center">Data: {{date('d-m-Y')}} Hora:{{date('H:i:s')}}</h5>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <table class="table table-bordered table-sm">
            <thead class="thead-light">
                <tr>
                    <th>Jogo</th>
                    <th>Desenvolvedor</th>
                    <th>Preço R$:</th>
                    <th>Foto</th>
                </tr>
            </thead>
            <tbody>
                @foreach($jogos as $jogo)
                    <tr>
                        <td>{{$jogo->titulo}}</td>
                        <td>{{$jogo->desenvolvedor->nome}}</td>
                        <td style="text-align: right">{{number_format($jogo->preco, 2, ',', '.')}}</td>
                        <td style="text-align: center">
                            <img src="{{public_path('storage/'.$jogo->foto)}}" style="width:100px;height:60px">         
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
</div>