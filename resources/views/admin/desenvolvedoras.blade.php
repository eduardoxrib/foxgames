@extends('adminlte::page')

@section('title', 'FoxGames - Categorias')

@section('content_header')

@endsection

@section('content')
    <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-8">
    <h1 style="text-align: center; ">Desenvolvedoras</h1>

    <h3>Inclusão de Desenvolvedoras</h3>
    <form method="post" action="{{ route('desenvolvedora.store') }}">
        {{ csrf_field() }}
        <label for="nome">Nome:</label>
        <div class="row">
            <div class="col-sm-10">
                <div class="form-group">
                    <input type="text" class="form-control" id="nome" name="nome">
                </div>
            </div>
            <div class="col-sm-2">
                <input type="submit" class="btn btn-warning" value="Adicionar">
            </div>
        </div>
    </form>


    <table class="table">
    <thead class="thead-dark">
      <tr>
        <th>ID</th>
        <th>NOME</th>
        <th>AÇÕES</th>
      </tr>
    </thead>
    <tbody>
      
      @foreach ($desenvolvedorasDB as $desenvolvedora)
        <tr>
          <td>{{$desenvolvedora->id}}</td>
          <td>{{$desenvolvedora->nome}}</td>
          <td>
            <form method="post" action="{{ route('desenvolvedora.destroy', $desenvolvedora->id)}}" style="display: inline-block" 
              onsubmit="return confirm('Confirma Exclusão da Desenvolvedora?')">          
              {{ method_field('delete') }}
              {{ csrf_field() }}
            <input type="submit" class="btn btn-danger btn-sm" style="float: right" value="Excluir">
            </form>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  <div class="col-sm-2"></div>

@endsection