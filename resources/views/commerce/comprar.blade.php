@extends('commerce/modelo')

@section('conteudo')

    <div class="container" style="margin-top: 5%">
        <div class="row">
            <div class="col-sm-6">
                <h2 style="text-align: center">{{$jogo->titulo}}</h2>
                <h3 style="text-align: center; color: blue">R$ {{ number_format($jogo->preco, 2) }}</h3>
                <form method="post" action="{{ route('site.insere') }}">
                    {{ csrf_field() }}

                    <label for="nome">Nome:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" id="nome" name="nome">
                    </div>

                    <label for="email">E-mail:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" id="email" name="email">
                    </div>

                        <input type="hidden" name="jogo_id" id="jogo_id" value="{{$jogo->id}}">
                        <input type="hidden" name="preco" id="preco" value="{{$jogo->preco}}">
                    <input type="submit" class="btn btn-success" style="width: 100%;" value="Finalizar Compra">
                </form>
            </div>
            <div class="col-sm-6">
                <img src="{{ asset('storage/'.$jogo->foto) }}" style="width: 100%" alt="jogo">
            </div>
        </div>
    </div>

@endsection