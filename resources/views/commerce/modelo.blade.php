<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FoxGames</title>
</head>
<body>

    <style>
        body {
            background-image: url('../storage/fotos/logo.png');
            background-repeat: no-repeat;
            background-size: 20%;
        }
    </style>

    <div class="container-fluid">
        <div class="col-sm-12">
            <div style="text-align: center; font-family: cursive, Gill Sans Extrabold, 
                sans-serif; color: black; font-size: 600%; margin-top: 2%">
            FoxGames
            </div>
        </div>
    </div>

    @yield('conteudo')

    <!-- <h5>footer</h5> -->
</body>
</html>



