@extends('commerce/modelo')

@section('conteudo')

    <form action="{{route('site.pesquisa')}}" style="margin-top: 3%">
        <label for="pesquisa">Pesquisar:</label>
        <input type="text" name="pesquisa" id="pesquisa">
        <input type="submit" value="Buscar" class="btn btn-success">
    </form>

    <div class="container" style="margin-top: 4%">
        <div class="row">
            @foreach ($jogos as $jogo)
                <div class="col-sm-4" style="margin-bottom: 1%; text-align: center">
                    <div class="card" style="width: 320px;">
                        <img class="card-img-top" src="{{ asset('storage/'.$jogo->foto) }}" alt="Card image cap" style="width: 320px; height: 280px; align:center">
                        <div class="card-body" style="width: 320px">
                            <h5 class="card-title" style="text-align: center; width: 320px"><b>{{$jogo->titulo}}</b></h5>
                            <p class="card-text" style="text-align: center"><b>{{$jogo->desenvolvedor->nome}}</b> <br/> R$ {{number_format($jogo->preco, 2)}}</p>
                            <div style="text-align: center;">
                                <a href="{{ route('site.compra', $jogo->id) }}" class="btn btn-info">Comprar</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection