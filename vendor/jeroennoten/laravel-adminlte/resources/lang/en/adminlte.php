<?php

return [

    'full_name'                   => 'Nome Completo',
    'email'                       => 'Email',
    'password'                    => 'Senha',
    'retype_password'             => 'Digite sua senha novamente',
    'remember_me'                 => 'Lembrar-me',
    'register'                    => 'Registrar',
    'register_a_new_membership'   => 'Registrar um novo membro',
    'i_forgot_my_password'        => 'Esqueci minha senha',
    'i_already_have_a_membership' => 'Eu já sou um membro',
    'sign_in'                     => 'Entrar',
    'log_out'                     => 'Sair',
    'toggle_navigation'           => 'Alterar navegação',
    'login_message'               => 'Faça o login para iniciar sua sessão',
    'register_message'            => 'Registrar um novo membro',
    'password_reset_message'      => 'Resetar senha',
    'reset_password'              => 'Resetar senha',
    'send_password_reset_link'    => 'Envie link para resetar a senha',
];
