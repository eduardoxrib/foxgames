# FoxGames

 Projeto em desenvolvimento para a cadeira "Programação para Internet II", Faculdade de Tecnologia Senac Pelotas, Graduação em Análise e Desenvolvimento de Sistemas. O projeto é desenvolvido em php, com Laravel, utilizando banco de dados MySQL. Para a página de administrador utilizo AdminLTE,em cliente estou utilizando Bootstrap 4.
 FoxGames é um projeto para estudo de Laravel, a ideia é que o cliente possa gerenciar o cadastro de jogos no sistema (gerenciar individualmente as categorias e idiomas de cada game) e obter informações com relatórios e gráficos.
 
 Tecnologias:
 
 *  Laravel 5.8
 *  Laravel DOMPDF
 *  MySQL
 *  Boostrap 4
 *  AdminLTE
 *  Google Charts v.46 ('current')

 Requisitos para rodar:
 
 1.  MySQL
 2.  Apache
 3.  Composer

 Executando:

 1.  Faça o download do repositório, ou clone do projeto para uma pasta de sua preferência (Necessário Git instalado)
 2.  Execute o MySQL e o Apache. Recomenda-se o xampp ou wamp instalado para uma execução mais fácil
 3.  Crie um banco de dados chamado "foxgames". Para mais informações: (https://www.devmedia.com.br/primeiros-passos-no-mysql/28438)
 4.  Navegue até a pasta raiz do projeto pelo prompt de comando
 5.  Execute o comando: "php artisan serve"
 6.  Abra o navegador de sua preferência
 7.  Coloque o endereço: "localhost:8000"