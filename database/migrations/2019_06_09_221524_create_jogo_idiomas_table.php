<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJogoIdiomasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jogo_idiomas', function (Blueprint $table) {
            $table->integer('jogo_id')->unsigned();
            // $table->foreign('jogo_id')->references('id')->on('jogos');
            $table->integer('idioma_id')->unsigned();
            // $table->foreign('idioma_id')->references('id')->on('idiomas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jogo_idiomas');
    }
}
