<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Jogo;

class CompraMail extends Mailable
{
    use Queueable, SerializesModels;

    public $nome;
    public $email;
    public $jogo_id;

    public function __construct($nome, $email, $jogo_id)
    {
        $this->nome = $nome;
        $this->email = $email;
        $this->jogo_id = $jogo_id;
    }

    public function build()
    {
        $jogo = Jogo::find($this->jogo_id);
        return $this->from('eduardoxrib@gmail.com')
                    ->subject('Mensagem enviada pelo site.')
                    ->view('commerce/mail', ['nome'=>$this->nome, 'jogo'=>$jogo]);
    }
}