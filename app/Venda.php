<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venda extends Model
{
    protected $fillable = ['nome', 'email', 'jogo_id', 'preco'];

    public function jogo() {
        return $this->belongsTo(Jogo::class);
    }
}
