<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Desenvolvedora;

use App\Jogo;
use App\Idioma;
use App\Categoria;
use App\Venda;

class JogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dados = Jogo::orderBy('titulo')->paginate(3);
        return view('admin/listagem_jogo', ['jogos' => $dados]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $desenvolvedores = Desenvolvedora::orderBy('nome')->get();
        return view('admin/cadastro_jogo', ['desenvolvedores' => $desenvolvedores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'titulo' => 'required|min:3|max:90',
            'preco' => 'required',
            'foto' => 'required'
        ]);

        $dados = $request->all();

        $caminhoFoto = $request->file('foto')->store('fotos', 'public');

        $dados['foto'] = $caminhoFoto;

        $cria = Jogo::create($dados);

        return redirect()->route('jogo.create')->with('status', 'Jogo cadastro com sucesso! =)');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dado = Jogo::find($id);
        $idiomas = Idioma::orderBy('nome')->get();
        $categorias = Categoria::orderBy('nome')->get();
        $desenvolvedores = Desenvolvedora::orderBy('nome')->get();
        return view('admin/pagina_jogo', ['jogo' => $dado, 'idiomas' => $idiomas, 'categorias' => $categorias
            , 'desenvolvedores' => $desenvolvedores]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dados = $request->all();

        $reg = Jogo::find($id);

        if (isset($dados['foto'])) {
          $path = $request->file('foto')->store('fotos', 'public');

          $dados['foto'] = $path; 

          $antiga = $reg->foto;
          Storage::disk('public')->delete($antiga);  
        }

        $alt = $reg->update($dados);

        if ($alt) {
            return redirect()->route('jogo.show', $reg->id)
                   ->with('status', 'Jogo alterado com sucesso');
        } else {
            return redirect()->route('jogo.show', $reg->id)
                   ->with('status', 'Ocorreu um erro, e o Jogo não foi alterado com sucesso');
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jogo = Jogo::find($id);

        $foto = $jogo->foto;

        Storage::disk('public')->delete($foto);

        $dest = Jogo::destroy($id);

        return redirect()->route('jogo.index');
    }

    public function add_idioma(Request $request) 
    {
        $id = $request->get('jogo_id');
        $idioma_id = $request->get('idioma_id');

        $jogo = Jogo::find($id);
        
        $idioma = Idioma::find($idioma_id);

        $jogo->idiomas()->attach($idioma);

        return redirect()->route('jogo.show', $jogo->id);
    }

    public function add_categoria(Request $request)
    {
        $id = $request->get('jogo_id');
        $categoria_id = $request->get('categoria_id');

        $jogo = Jogo::find($id);
        
        $categoria = Categoria::find($categoria_id);

        $jogo->categorias()->attach($categoria);

        return redirect()->route('jogo.show', $jogo->id);
    }

    public function home_graficos() {
        $dados = Jogo::selectRaw('desenvolvedor.nome as dev, count(*) as num')
        ->join('desenvolvedor', 'desenvolvedor_id', '=', 'desenvolvedor.id')
        ->groupBy('dev')
        ->get();

        $vendas = Venda::orderBy('updated_at', 'DESC')->get();

        return view('admin/home', ['jogos' => $dados, 'vendas' => $vendas]);
    }

    public function reljogos () {
        $jogos = Jogo::orderBy('titulo')->get();
        return \PDF::loadView('admin/relJogos', ['jogos'=>$jogos])
        ->stream();
    }

}
