<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\CompraMail;

use App\Jogo;
use App\Venda;

class CommerceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dados = Jogo::orderBy('titulo')->get();
        
        return view('commerce/home', ['jogos' => $dados]);
    }

    public function compra($id)
    {
        $jogo = Jogo::find($id);
        return view('commerce/comprar', ['jogo' => $jogo]);
    }

    public function insere(Request $request)
    {
        $dados = $request->all();
        $create = Venda::create($dados);

        $nome = $request->get('nome');
        $email = $request->get('email');
        $jogo_id = $request->get('jogo_id');

        Mail::to($email)->send(new CompraMail($nome, $email, $jogo_id));

        return redirect()->route('site.index');
    }

    public function pesquisa (Request $request) {
        $pesquisa = $request->get('pesquisa');
        $resultado = Jogo::orderBy('titulo')->where('titulo', 'like','%' . $pesquisa . '%')->get();
        return view('commerce/pesquisa', ['jogos' => $resultado]);
    }
}
