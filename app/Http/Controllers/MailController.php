<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\EmailController;
use App\Mail\CompraMail;

class MailController extends Controller
{
    public function enviaEmail() {
        $destinatario = "eduardoxrib@gmail.com";
        Mail::to($destinatario)->send(new CompraMail());
    }
}
