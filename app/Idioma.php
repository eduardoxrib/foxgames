<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idioma extends Model
{
    protected $fillable = ['nome'];

    public function jogos() {
        return $this->belongsToMany(Jogo::class, 'jogo_idiomas');
    }
}
