<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desenvolvedora extends Model
{
    protected $fillable = ['nome'];

    protected $table = "desenvolvedor";
}
