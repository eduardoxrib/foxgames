<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $fillable = ['nome'];

    public function jogos() {
        return $this->belongsToMany(Jogo::class, 'jogo_categorias');
    }
}
