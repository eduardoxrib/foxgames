<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Desenvolvedora;

class Jogo extends Model
{
    protected $fillable = ['titulo', 'preco', 'foto', 'desenvolvedor_id'];

    public function desenvolvedor() 
    {
        return $this->belongsTo(Desenvolvedora::class, 'desenvolvedor_id');
    }

    public function idiomas() {
        return $this->belongsToMany(Idioma::class, 'jogo_idiomas');
    }

    public function categorias() {
        return $this->belongsToMany(Categoria::class, 'jogo_categorias');
    }

    public function setPrecoAttribute($value) {
        $novo1 = str_replace('.', '', $value);
        $novo2 = str_replace(',', '.', $novo1);
        $this->attributes['preco'] = $novo2;
    }

}
