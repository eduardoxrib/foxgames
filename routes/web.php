<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return redirect()->route('jogo.graficos_home');
})->middleware('auth');

Route::get('/home', function () {
    return view('admin/home');
})->middleware('auth');

Route::get('admin/home', function () {
    return redirect()->route('jogo.graficos_home');
})->middleware('auth');

Route::get('admin/idiomas', function () {
    return redirect()->route('idiomas.index');
})->middleware('auth');

Route::get('admin/categorias', function () {
    return redirect()->route('categoria.index');
})->middleware('auth');

Route::get('admin/desenvolvedora', function () {
    return redirect()->route('desenvolvedora.index');
})->middleware('auth');

Route::get('admin/jogo', function () {
    return redirect()->route('jogo.create');
})->middleware('auth');

Route::get('admin/listagem_jogo', function() {
    return redirect()->route('jogo.index');
})->middleware('auth');

Route::get('admin/rel', function() {
    return redirect()->route('jogo.reljogos');
});

Route::post('admin/add_idioma_jogo', 'JogoController@add_idioma')->name('jogo.add_idioma')->middleware('auth');
Route::post('admin/add_categoria_jogo', 'JogoController@add_categoria')->name('jogo.add_categoria')->middleware('auth');
Route::get('admin/home_graficos', 'JogoController@home_graficos')->name('jogo.graficos_home')->middleware('auth');
Route::get('admin/relatorio', 'JogoController@reljogos')->name('jogo.reljogos')->middleware('auth');

Route::resource('jogo', 'JogoController')->middleware('auth');
Route::resource('idiomas', 'IdiomaController')->middleware('auth');
Route::resource('categoria', 'CategoriaController')->middleware('auth');
Route::resource('desenvolvedora', 'DesenvolvedoraController')->middleware('auth');

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------

Route::get('/email', 'MailController@enviaEmail');

Route::get('/site', 'CommerceController@index')->name('site.index');
Route::post('/site/insere', 'CommerceController@insere')->name('site.insere');

Route::get('compra/{id}', 'CommerceController@compra')->where(['id' => '[0-9]+'])->name('site.compra');
Route::get('pesquisa', 'CommerceController@pesquisa')->name('site.pesquisa');

Route::get('/commerce', function() {
    return redirect()->route('site.index');
});


